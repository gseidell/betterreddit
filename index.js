const request = require('request');
var express = require('express');
var bodyParser = require('body-parser');
var exphbs = require('express-handlebars');
var async = require('async');
var buildUrl = require('build-url');
var urlParse = require('url');

var app = express();

//Path and port dictated by nginx configuration
//make "" for testing with localhost
var path = "";
var port = 3000;
var imgurHeader = { "Authorization": "Client-ID 58432e0759aaf62" }


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.engine('handlebars', exphbs({ defaultLayout: 'header' }));
app.set('view engine', 'handlebars');
app.use(path + '/public', express.static('public'));

var toSend = [];
var albumsToSend = [];

//makes sure all are direct media links
function washUrl(toClean) {
    if (toClean.includes(".gifv"))
        return toClean.replace(".gifv", "/embed")
    if (!/\.\w+$/.exec(toClean))
        return toClean + ".jpg";
}

function handleRequest(req, res) {

    //Parse initial url and prepare reddit request
    var url = `http://www.reddit.com/r/${req.params.subreddit}`;
    url += req.params.sort ? `/${req.params.sort}.json` : "/.json";

    var initialReuqest = { path: req.path, query: req.query }
    url = buildUrl(url, { queryParams: req.query });


    //Make initial reddit request               
    //                      vvvv bound at the end of function
    request(url, function (passedInInitialRequest, err, response, body) {
        // console.log(arguments[0]);
        redditData = JSON.parse(response.body).data;

        // after = redditData.after;

        passedInInitialRequest.query.after = redditData.after;

        var urlToBePassed = buildUrl(passedInInitialRequest.path, { queryParams: passedInInitialRequest.query });

        var a = redditData.children.map(function (x) {
            if (!x.data.is_self)
                return {
                    title: x.data.title,
                    urls: [x.data.url],
                    score: x.data.score,
                    subreddit: x.data.subreddit
                }
        })
        //Clear out selfposts
        a = a.filter(x => x != undefined)

        //Check for Imgur galleries
        var albums = a.filter(x => x.urls[0].includes(".com/a/") || x.urls[0].includes(".com/gall"))
        if (albums == []) {
            res.render("list", { data: a, nextLink: urlToBePassed });
        } else {
            //IMGUR Gallery
            toSend = a;
            //Make imgur api request for each gallery
            async.each(albums, function (album, callback) {
                var cleanUrl = urlParse.parse(album.urls[0])  //URL object
                cleanUrl = cleanUrl.origin + cleanUrl.pathname; //string
                var galleryHash = /\w+$/.exec(cleanUrl)[0];
                var requestOptions = {
                    url: "https://api.imgur.com/3/album/" + galleryHash,
                    headers: imgurHeader
                };
                request(requestOptions, function (err2, response2, body2) {
                    var albumData = JSON.parse(response2.body).data.images;
                    albumUrls = albumData.map(image => image.link);
                    albumsToSend = [...albumsToSend,
                    {
                        title: album.title, urls: albumUrls,
                        score: album.score, subreddit: album.subreddit
                    }];
                    callback(); //I honestly dont understand why this is necessary
                })

            }, function (err) {

                //Replace Album urls with corresponding list of urls
                for (var i = 0; i != toSend.length; i++) {
                    for (var j = 0; j != albumsToSend.length; j++) {
                        if (toSend[i].title == albumsToSend[j].title) {
                            toSend[i] = albumsToSend[j];
                        }
                    }
                }
                res.render("list", { data: toSend, nextLink: urlToBePassed });
            });
        }
    }.bind(null, initialReuqest));
}

app.get(path + '/r/:subreddit/:sort', handleRequest);

app.get(path + '/r/:subreddit', handleRequest);

app.get(path + '', function (req, res) {
    res.render("home", { layout: false });
});


app.listen(port, function () {
    console.log('Listening on port 3000!');
});
